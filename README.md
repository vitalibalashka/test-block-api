### Test REST Api for retrieving blockchain blocks

### How to run:

```
make build && make run
```

### Available endpoints:

| endpoint             | description                              |
|----------------------|------------------------------------------|
| ` /getBlock/:height` | provides block by height                 |
| ` /all`              | provides all blocks already stored in db |
