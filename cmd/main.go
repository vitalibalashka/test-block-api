package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	test_block_api "test-block-api"
	"test-block-api/pkg/handler"
	"test-block-api/pkg/repository"
	"test-block-api/pkg/service"
)

func main() {
	db := repository.NewPostgresDB()

	repositories := repository.NewRepository(db)
	services := service.NewService(repositories)
	handlers := handler.NewHandler(services)

	srv := new(test_block_api.Server)
	go func() {
		if err := srv.Run(8000, handlers.InitRoutes()); err != nil {
			log.Fatalf("error occured while running http server: %s", err.Error())
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	log.Println("Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		log.Fatalf("error occured on server shutting down: %s", err.Error())
	}
}
