package service

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"test-block-api/pkg/models"
	"test-block-api/pkg/repository"
)

type BlockService struct {
	repository repository.BlockRepository
}

func (b BlockService) Add(block models.Block) (models.Block, error) {
	return b.repository.Add(block)
}

func (b BlockService) GetByHeight(height int) (models.Block, error) {
	block, err := b.repository.GetByHeight(height)
	if err == nil {
		return block, err
	}
	block, err = b.GetByHeightRest(height)
	if err != nil {
		return block, err
	}
	return b.Add(block)
}

func (b BlockService) GetByHeightRest(height int) (models.Block, error) {
	url := fmt.Sprintf("https://api.whatsonchain.com/v1/bsv/main/block/height/%d", height)
	resp, err := http.Get(url)
	if err != nil {
		return models.Block{}, err
	}
	body, _ := io.ReadAll(resp.Body)
	var block models.Block
	err = json.Unmarshal(body, &block)

	return block, err
}

func (b BlockService) GetAll() ([]models.Block, error) {
	return b.repository.GetAll()
}

func NewBlockService(repo repository.BlockRepository) *BlockService {
	return &BlockService{repository: repo}
}
