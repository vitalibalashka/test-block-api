package service

import (
	"test-block-api/pkg/models"
	"test-block-api/pkg/repository"
)

type Block interface {
	Add(block models.Block) (models.Block, error)
	GetByHeight(height int) (models.Block, error)
	GetByHeightRest(height int) (models.Block, error)
	GetAll() ([]models.Block, error)
}

type Service struct {
	Block
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Block: NewBlockService(repos.BlockRepository),
	}
}
