package models

import "github.com/lib/pq"

type Block struct {
	Id                int            `json:"-" gorm:"primaryKey;autoIncrement:true;unique"`
	Hash              string         `json:"hash"`
	Confirmations     int            `json:"confirmations"`
	Size              int            `json:"size"`
	Height            int            `json:"height"`
	Version           int            `json:"version"`
	VersionHex        string         `json:"versionHex"`
	Merkleroot        string         `json:"merkleroot"`
	Txcount           int            `json:"txcount"`
	NTx               int            `json:"nTx"`
	NumTx             int            `json:"num_tx"`
	Tx                pq.StringArray `json:"tx" gorm:"type:text[]"`
	Time              int            `json:"time"`
	Mediantime        int            `json:"mediantime"`
	Nonce             int64          `json:"nonce"`
	Bits              string         `json:"bits"`
	Difficulty        float64        `json:"difficulty"`
	Chainwork         string         `json:"chainwork"`
	Previousblockhash string         `json:"previousblockhash"`
	Nextblockhash     string         `json:"nextblockhash"`
	CoinbaseTx        CoinbaseTx     `json:"coinbaseTx" gorm:"foreignKey:BlockRefer;references:Id"`
	TotalFees         float64        `json:"totalFees"`
	//TODO
	// 22021: invalid byte sequence for encoding "UTF8"
	// Need to fix later
	//Miner             string       `json:"miner"`
	//TODO
	// Need to find type for pages
	//Pages				?			 `json:"pages"`

}
