package models

type CoinbaseTx struct {
	Id            int    `json:"-" gorm:"primaryKey;autoIncrement:true;unique"`
	Txid          string `json:"txid" gorm:"type:text"`
	Hash          string `json:"hash"`
	Version       int    `json:"version"`
	Size          int    `json:"size"`
	Locktime      int    `json:"locktime"`
	Vin           []Vin  `json:"vin" json:"coinbaseTx" gorm:"foreignKey:CoinbaseTxRefer;references:Id"`
	Vout          []Vout `json:"vout" json:"coinbaseTx" gorm:"foreignKey:CoinbaseTxRefer;references:Id"`
	Blockhash     string `json:"blockhash"`
	Confirmations int    `json:"confirmations"`
	Time          int    `json:"time"`
	Blocktime     int    `json:"blocktime"`
	Blockheight   int    `json:"blockheight"`
	BlockRefer    int    `json:"-"`
}
