package models

type Vout struct {
	Id              int          `json:"-" gorm:"primaryKey;autoIncrement:true;unique"`
	Value           float64      `json:"value"`
	N               int          `json:"n"`
	ScriptPubKey    ScriptPubKey `json:"scriptPubKey" gorm:"foreignKey:VoutRefer;references:Id"`
	CoinbaseTxRefer int          `json:"-"`
}
