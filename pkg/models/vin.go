package models

type Vin struct {
	Id              int       `json:"-" gorm:"primaryKey;autoIncrement:true;unique"`
	Coinbase        string    `json:"coinbase"`
	Txid            string    `json:"txid"`
	Vout            int       `json:"vout"`
	ScriptSig       ScriptSig `json:"scriptSig" gorm:"foreignKey:VinRefer;references:Id"`
	Sequence        int64     `json:"sequence"`
	CoinbaseTxRefer int       `json:"-"`
}
