package models

type ScriptSig struct {
	Id       int    `json:"-" gorm:"primaryKey;autoIncrement:true;unique"`
	Asm      string `json:"asm"`
	Hex      string `json:"hex"`
	VinRefer int    `json:"-"`
}
