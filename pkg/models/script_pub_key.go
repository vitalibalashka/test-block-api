package models

import (
	pq "github.com/lib/pq"
)

type ScriptPubKey struct {
	Id          int            `json:"-" gorm:"primaryKey;autoIncrement:true;unique"`
	Asm         string         `json:"asm"`
	Hex         string         `json:"hex"`
	ReqSigs     int            `json:"reqSigs"`
	Type        string         `json:"type"`
	Addresses   pq.StringArray `json:"addresses" gorm:"type:text[]"`
	IsTruncated bool           `json:"isTruncated"`
	VoutRefer   int            `json:"-"`
}
