package repository

import (
	"gorm.io/gorm"
	"test-block-api/pkg/models"
)

type BlockPostgres struct {
	db *gorm.DB
}

func (b BlockPostgres) Add(block models.Block) (models.Block, error) {
	dbc := b.db.Create(&block)
	return block, dbc.Error
}

func (b BlockPostgres) GetByHeight(height int) (models.Block, error) {
	var block = models.Block{}
	dbc := b.db.
		Preload("CoinbaseTx").
		Preload("CoinbaseTx.Vin").
		Preload("CoinbaseTx.Vout").
		Preload("CoinbaseTx.Vout.ScriptPubKey").
		Preload("CoinbaseTx.Vin.ScriptSig").
		Where(&models.Block{Height: height}).
		First(&block)
	return block, dbc.Error
}

func (b BlockPostgres) GetAll() ([]models.Block, error) {
	blocks := []models.Block{}
	dbc := b.db.Preload("CoinbaseTx").
		Preload("CoinbaseTx.Vin").
		Preload("CoinbaseTx.Vout").
		Preload("CoinbaseTx.Vout.ScriptPubKey").
		Preload("CoinbaseTx.Vin.ScriptSig").
		Find(&blocks)
	return blocks, dbc.Error
}

func NewBlockPostgresRepo(db *gorm.DB) *BlockPostgres {
	return &BlockPostgres{db: db}
}
