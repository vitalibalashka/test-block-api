package repository

import (
	"gorm.io/gorm"
	"test-block-api/pkg/models"
)

type BlockRepository interface {
	Add(block models.Block) (models.Block, error)
	GetByHeight(height int) (models.Block, error)
	GetAll() ([]models.Block, error)
}

type Repository struct {
	BlockRepository
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		BlockRepository: NewBlockPostgresRepo(db),
	}
}
