package handler

import (
	"github.com/gin-gonic/gin"
	"test-block-api/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	router.GET("/getBlock/:height", h.getBlock)
	router.GET("/all", h.getAllBlocksFromDB)

	return router
}
