package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func (h *Handler) getBlock(c *gin.Context) {
	height, err := strconv.Atoi(c.Param("height"))
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	block, err := h.services.Block.GetByHeight(height)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, block)
}

func (h *Handler) getAllBlocksFromDB(c *gin.Context) {
	blocks, err := h.services.Block.GetAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, blocks)
}
